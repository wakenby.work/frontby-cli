#!/usr/bin/env node

const commander = require('commander')

const fs = require('fs')
const AdmZip = require('adm-zip')
const request = require('request')
const mkdirp = require('mkdirp')
const chalk = require('chalk')

const file_url = 'https://gitlab.com/wakenby.work/frontby/-/archive/main/frontby-main.zip'

commander
  .version('1.0.0')
  .description('frontby - cli система для генерации компонентов и развертывании проекта')

function templateScss(name) {
  return `.${name} {\n}\n`
}

function templatePug(name) {
  return `mixin ${name}()\n`
}

function issetBem(name) {
  return (
    fs.existsSync(`src/modules/${name}`) || fs.existsSync(`src/styles/common/${name}.scss`)
  )
}

function messageIssetBem () {
  return console.log(
    chalk.red('Bem компонент уже существует')
  )
}

commander
  .command('init')
  .description('Развертывании нового проекта')
  .alias('i')
  .action(() => {
    request.get({url: file_url, encoding: null}, (err, res, body) => {
      const zip = new AdmZip(body)
      const zipEntries = zip.getEntries()

      for (let i = 0; i < zipEntries.length; i++) {
        zipEntries[i].entryName = zipEntries[i].entryName.replace('frontby-main', '/')
      }

      zip.extractAllTo('', true)
    })
  })

commander
  .command('modules <name>')
  .description('Создание модуля')
  .alias('m')
  .action((name) => {
    if (issetBem(name)) return messageIssetBem()

    mkdirp.sync(`src/modules/${name}`)

    fs.writeFileSync(`src/modules/${name}/${name}.scss`, templateScss(name))
    fs.writeFileSync(`src/modules/${name}/${name}.pug`, templatePug(name))
  })

commander
  .command('common <name>')
  .description('Создание scss в common')
  .alias('c')
  .action((name) => {
    if (issetBem(name)) return messageIssetBem()

    fs.writeFileSync(`src/styles/common/${name}.scss`, templateScss(name))
  })

commander.parse(process.argv)
